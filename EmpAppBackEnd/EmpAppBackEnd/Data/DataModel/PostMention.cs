﻿namespace EmpAppBackEnd.Data.DataModel
{
    public class PostMention : BaseModel
    {
        public int PostId { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
    }
}

﻿using EmpAppBackEnd.Data.Abstract;

namespace EmpAppBackEnd.Data.DataModel
{
    public class PostReaction : IEntityBase
    {
        public int Id { get; set; }
        public int PostId { get; set; }
        public int UserId { get; set; }

        public User User { get; set; }
    }
}

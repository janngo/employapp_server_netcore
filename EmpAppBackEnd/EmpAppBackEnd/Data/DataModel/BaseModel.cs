﻿using System;
using System.ComponentModel.DataAnnotations;
using EmpAppBackEnd.Data.Abstract;

namespace EmpAppBackEnd.Data.DataModel
{
    public abstract class BaseModel : IEntityBase
    {
        [Key]
        public int Id { get; set; }
        
        public DateTime CreatedTime { get; set; }
        public DateTime UpdatedTime { get; set; }
    }
}

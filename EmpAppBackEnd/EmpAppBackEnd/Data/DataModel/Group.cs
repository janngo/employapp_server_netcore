﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EmpAppBackEnd.Data.DataModel
{
    public class Group : BaseModel
    {
        public Group()
        {
            GroupPermissions = new List<GroupPermission>();
        }

        [MaxLength(50)]
        [MinLength(1)]
        public string Name { get; set; }
        public string EnglishName { get; set; }
        public string Description { get; set; }
        public string GroupExternalIdentify { get; set; }
        public ICollection<GroupPermission> GroupPermissions { get; set; }
    }
}

﻿namespace EmpAppBackEnd.Data.DataModel
{
    public class GroupMember
    {
        public int UserId { get; set; }
        public int GroupId { get; set; }
        public Group Group { get; set; }
    }
}

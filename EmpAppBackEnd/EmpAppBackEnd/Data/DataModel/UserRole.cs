﻿using Microsoft.AspNetCore.Identity;

namespace EmpAppBackEnd.Data.DataModel
{
    public class UserRole : IdentityRole<int>
    {
        public UserRole() : base()
        {
            
        }

        public UserRole(string name): base(name)
        { }
    }
}

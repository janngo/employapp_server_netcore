﻿namespace EmpAppBackEnd.Data.DataModel
{
    public class PostVideo : BaseModel
    {
        public int PostId { get; set; }
        public string Path { get; set; }
        public string ThumbnailPath { get; set; }
    }
}

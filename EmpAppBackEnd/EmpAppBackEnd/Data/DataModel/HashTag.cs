﻿using System.ComponentModel.DataAnnotations;

namespace EmpAppBackEnd.Data.DataModel
{
    public class HashTag : BaseModel
    {
        [MaxLength(50)]
        [MinLength(1)]
        public string Content { get; set; }
        public int UsedNo { get; set; }
    }
}

﻿namespace EmpAppBackEnd.Data.DataModel
{
    public class PostImage : BaseModel
    {
        public int PostId { get; set; }
        public string Path { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace EmpAppBackEnd.Data.DataModel
{
    public class Report: BaseModel
    {
        public int UserId { get; set; }
        public int TargetType { get; set; }
        public int TargetId { get; set; }
        [MaxLength(200)]
        public string Comment { get; set; }
    }
}

﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EmpAppBackEnd.Data.DataModel
{
    public class Post : BaseModel
    {
        public Post()
        {
            Comments = new List<Comment>();
            Images = new List<PostImage>();
            Videos = new List<PostVideo>();
            Links = new List<PostLink>();
            Reactions = new List<PostReaction>();
            HashTags = new List<PostHashtTag>();
        }

        [MaxLength(100)]
        [MinLength(1)]
        public string Title { get; set; }
        [MaxLength(1000)]
        [MinLength(1)]
        public string Content { get; set; }
        public int GroupId { get; set; }
        public int CreatedUserId { get; set; }
        public string Tags { get; set; }
        public bool IsPublish { get; set; }
        public int CommentCount { get; set; }
        public int InteractCount { get; set; }

        public User User { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public ICollection<PostImage> Images { get; set; }
        public ICollection<PostVideo> Videos { get; set; }
        public ICollection<PostLink> Links { get; set; }
        public ICollection<PostReaction> Reactions { get; set; }
        public ICollection<PostHashtTag> HashTags { get; set; }

    }
}

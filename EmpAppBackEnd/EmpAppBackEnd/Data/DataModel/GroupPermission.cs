﻿namespace EmpAppBackEnd.Data.DataModel
{
    public class GroupPermission : BaseModel
    {
        public int GroupId { get; set; }
        public bool IsRead { get; set; }
        public bool IsWrite { get; set; }
    }
}

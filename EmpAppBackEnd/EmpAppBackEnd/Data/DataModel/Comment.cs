﻿using System.ComponentModel.DataAnnotations;

namespace EmpAppBackEnd.Data.DataModel
{
    public class Comment : BaseModel
    {
        public int PostId { get; set; }
        public int UserId { get; set; }
        [MaxLength(200)]
        [MinLength(1)]
        public string Content { get; set; }

        public User User { get; set; }
    }
}

﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EmpAppBackEnd.Data.Abstract;
using Microsoft.AspNetCore.Identity;

namespace EmpAppBackEnd.Data.DataModel
{
    public class User : IdentityUser<int>,IEntityBase
    {
        [MaxLength(30)]
        public string FirstName { get; set; }
        [MaxLength(30)]
        public string LastName { get; set; }
        [MaxLength(50)]
        public string Position { get; set; }
        public string Avartar { get; set; }

        public ICollection<GroupMember> GroupMember { get; set; }
    }
}

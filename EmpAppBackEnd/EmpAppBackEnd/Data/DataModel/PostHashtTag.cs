﻿using System.Collections.Generic;

namespace EmpAppBackEnd.Data.DataModel
{
    public class PostHashtTag
    {
        public int PostId { get; set; }
        public int HashTagId { get; set; }

        public ICollection<HashTag> HashTags { get; set; }
    }
}

﻿namespace EmpAppBackEnd.Data.Abstract
{
    public interface IEntityBase
    {
        int Id { get; set; }
    }
}

﻿using System.Collections.Generic;
using EmpAppBackEnd.Data.DataModel;

namespace EmpAppBackEnd.Data.Abstract
{
    public interface IPostReactionRepository : IEntityBaseRepository<PostReaction>
    {
        ICollection<PostImage> GetByPostId(int postId);
    }
}

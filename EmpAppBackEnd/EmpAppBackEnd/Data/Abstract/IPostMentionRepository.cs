﻿using System.Collections.Generic;
using EmpAppBackEnd.Data.DataModel;

namespace EmpAppBackEnd.Data.Abstract
{
    public interface IPostMentionRepository : IEntityBaseRepository<PostMention>
    {
        ICollection<PostImage> GetByPostId(int postId);
    }
}

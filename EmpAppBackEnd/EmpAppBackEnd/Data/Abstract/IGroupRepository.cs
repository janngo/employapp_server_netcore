﻿using System.Collections.Generic;
using EmpAppBackEnd.Data.DataModel;

namespace EmpAppBackEnd.Data.Abstract
{
    public interface IGroupRepository:IEntityBaseRepository<Group>
    {
        ICollection<Group> GetAvailableGroup(int userId);
    }
}

﻿using EmpAppBackEnd.Data.DataModel;

namespace EmpAppBackEnd.Data.Abstract
{
    public interface IHashTagRepository : IEntityBaseRepository<HashTag>
    {
    }
}

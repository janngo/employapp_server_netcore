﻿using EmpAppBackEnd.Data.DataModel;

namespace EmpAppBackEnd.Data.Abstract
{
    public interface IPostRepository : IEntityBaseRepository<Post>
    {
    }
}

﻿using System.Collections.Generic;
using EmpAppBackEnd.Data.DataModel;

namespace EmpAppBackEnd.Data.Abstract
{
    public interface IPostVideoRepository : IEntityBaseRepository<PostVideo>
    {
        ICollection<PostImage> GetByPostId(int postId);
    }
}

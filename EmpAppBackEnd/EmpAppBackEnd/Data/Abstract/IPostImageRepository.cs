﻿using System.Collections.Generic;
using EmpAppBackEnd.Data.DataModel;

namespace EmpAppBackEnd.Data.Abstract
{
    public interface IPostImageRepository : IEntityBaseRepository<PostImage>
    {
        ICollection<PostImage> GetByPostId(int postId);
    }
}

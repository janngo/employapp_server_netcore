﻿using EmpAppBackEnd.Data.DataModel;

namespace EmpAppBackEnd.Data.Abstract
{
    public interface ICommentRepository : IEntityBaseRepository<Comment>
    {
    }
}

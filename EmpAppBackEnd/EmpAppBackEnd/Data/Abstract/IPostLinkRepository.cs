﻿using System.Collections.Generic;
using EmpAppBackEnd.Data.DataModel;

namespace EmpAppBackEnd.Data.Abstract
{
    public interface IPostLinkRepository : IEntityBaseRepository<PostLink>
    {
        ICollection<PostImage> GetByPostId(int postId);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EmpAppBackEnd.Data.Abstract;
using EmpAppBackEnd.Data.Context;
using EmpAppBackEnd.Data.DataModel;

namespace EmpAppBackEnd.Data.Repositories
{
    public class PostReactionRepository : EntityBaseRepository<PostReaction>,IPostReactionRepository
    {
        public PostReactionRepository(ApplicationDbContext context) : base(context)
        {
        }

        public ICollection<PostImage> GetByPostId(int postId)
        {
            throw new NotImplementedException();
        }
    }
}

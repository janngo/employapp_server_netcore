﻿using System;
using System.Collections.Generic;
using EmpAppBackEnd.Data.Abstract;
using EmpAppBackEnd.Data.Context;
using EmpAppBackEnd.Data.DataModel;

namespace EmpAppBackEnd.Data.Repositories
{
    public class PostVideoRepository : EntityBaseRepository<PostVideo>, IPostVideoRepository
    {
        public PostVideoRepository(ApplicationDbContext context) : base(context)
        {
        }

        public ICollection<PostImage> GetByPostId(int postId)
        {
            throw new NotImplementedException();
        }
    }
}

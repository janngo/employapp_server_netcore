﻿using EmpAppBackEnd.Data.Abstract;
using EmpAppBackEnd.Data.Context;
using EmpAppBackEnd.Data.DataModel;

namespace EmpAppBackEnd.Data.Repositories
{
    public class CommentRepository : EntityBaseRepository<Comment>, ICommentRepository
    {
        public CommentRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}

﻿using System;
using System.Collections.Generic;
using EmpAppBackEnd.Data.Abstract;
using EmpAppBackEnd.Data.Context;
using EmpAppBackEnd.Data.DataModel;

namespace EmpAppBackEnd.Data.Repositories
{
    public class GroupRepository : EntityBaseRepository<Group>,IGroupRepository
    {
        public GroupRepository(ApplicationDbContext context) : base(context)
        {
        }

        public ICollection<Group> GetAvailableGroup(int userId)
        {
            throw new NotImplementedException();
        }
    }
}

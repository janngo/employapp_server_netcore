﻿using EmpAppBackEnd.Data.Abstract;
using EmpAppBackEnd.Data.Context;
using EmpAppBackEnd.Data.DataModel;

namespace EmpAppBackEnd.Data.Repositories
{
    public class HashTagRepository : EntityBaseRepository<HashTag>,IHashTagRepository
    {
        public HashTagRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}

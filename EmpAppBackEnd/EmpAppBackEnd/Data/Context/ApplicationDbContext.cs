﻿using System;
using EmpAppBackEnd.Data.DataModel;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace EmpAppBackEnd.Data.Context
{
    public class ApplicationDbContext : IdentityDbContext<User,UserRole,int>
    {
        public DbSet<Post> Posts { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<PostImage> PostImages { get; set; }
        public DbSet<PostVideo> PostVideos { get; set; }
        public DbSet<PostLink> PostLinks { get; set; }
        public DbSet<PostReaction> PostReactions { get; set; }
        public DbSet<PostMention> PostMentions { get; set; }
        public DbSet<PostHashtTag> PostHashtTags { get; set; }
        public DbSet<HashTag> HashTags { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<GroupMember> GroupMembers { get; set; }
        public DbSet<GroupPermission> GroupPermissions { get; set; }
        public DbSet<Report> Reports { get; set; }
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);

            builder.Entity<Post>().ToTable("Post");
            builder.Entity<Post>().Property(s => s.CreatedUserId).IsRequired();
            builder.Entity<Post>().Property(s => s.GroupId).IsRequired();
            builder.Entity<Post>().Property(s => s.Content).IsRequired();
            builder.Entity<Post>().Property(s => s.IsPublish).HasDefaultValue(true).ValueGeneratedNever();
            builder.Entity<Post>().Property(s => s.CreatedTime).HasDefaultValue(DateTime.Now);
            builder.Entity<Post>().Property(s => s.UpdatedTime).HasDefaultValue(DateTime.Now);
            builder.Entity<Post>().Property(s => s.Tags).IsRequired(false);

            builder.Entity<Comment>().ToTable("Comment");
            builder.Entity<Comment>().Property(s => s.PostId).IsRequired();
            builder.Entity<Comment>().Property(s => s.UserId).IsRequired();
            builder.Entity<Comment>().Property(s => s.Content).IsRequired();
            builder.Entity<Comment>().Property(s => s.CreatedTime).HasDefaultValue(DateTime.Now);
            builder.Entity<Comment>().Property(s => s.UpdatedTime).HasDefaultValue(DateTime.Now);


            builder.Entity<PostMention>().ToTable("PostMention");
            builder.Entity<PostMention>().Property(s => s.PostId).IsRequired();
            builder.Entity<PostMention>().Property(s => s.UserId).IsRequired();
            builder.Entity<PostMention>().Property(s => s.CreatedTime).HasDefaultValue(DateTime.Now);
            builder.Entity<PostMention>().Property(s => s.UpdatedTime).HasDefaultValue(DateTime.Now);

            builder.Entity<PostImage>().ToTable("PostImage");
            builder.Entity<PostImage>().Property(s => s.PostId).IsRequired();
            builder.Entity<PostImage>().Property(s => s.Path).IsRequired();
            builder.Entity<PostImage>().Property(s => s.CreatedTime).HasDefaultValue(DateTime.Now);
            builder.Entity<PostImage>().Property(s => s.UpdatedTime).HasDefaultValue(DateTime.Now);

            builder.Entity<PostVideo>().ToTable("PostVideo");
            builder.Entity<PostVideo>().Property(s => s.PostId).IsRequired();
            builder.Entity<PostVideo>().Property(s => s.Path).IsRequired();
            builder.Entity<PostVideo>().Property(s => s.CreatedTime).HasDefaultValue(DateTime.Now);
            builder.Entity<PostVideo>().Property(s => s.UpdatedTime).HasDefaultValue(DateTime.Now);

            builder.Entity<PostLink>().ToTable("PostLink");
            builder.Entity<PostLink>().Property(s => s.PostId).IsRequired();
            builder.Entity<PostLink>().Property(s => s.Url).IsRequired();
            builder.Entity<PostLink>().Property(s => s.Title).IsRequired(false);
            builder.Entity<PostLink>().Property(s => s.Description).IsRequired(false);
            builder.Entity<PostLink>().Property(s => s.ThumbnailPath).IsRequired(false);
            builder.Entity<PostLink>().Property(s => s.CreatedTime).HasDefaultValue(DateTime.Now);
            builder.Entity<PostLink>().Property(s => s.UpdatedTime).HasDefaultValue(DateTime.Now);

            builder.Entity<PostReaction>().ToTable("PostReaction");
            builder.Entity<PostReaction>().HasKey("PostId", "UserId");
            builder.Entity<PostReaction>().Property(s => s.PostId).IsRequired();
            builder.Entity<PostReaction>().Property(s => s.UserId).IsRequired();
            
            builder.Entity<HashTag>().ToTable("HashTag");
            builder.Entity<HashTag>().Property(s => s.Content).IsRequired();
            builder.Entity<HashTag>().Property(s => s.CreatedTime).HasDefaultValue(DateTime.Now);
            builder.Entity<HashTag>().Property(s => s.UpdatedTime).HasDefaultValue(DateTime.Now);

            builder.Entity<PostHashtTag>().ToTable("PostHashtTag");
            builder.Entity<PostHashtTag>().HasKey("PostId", "HashTagId");
            builder.Entity<PostHashtTag>().Property(s => s.PostId).IsRequired();
            builder.Entity<PostHashtTag>().Property(s => s.HashTagId).IsRequired();

            builder.Entity<Group>().ToTable("Group");
            builder.Entity<Group>().Property(s => s.Name).IsRequired();
            builder.Entity<Group>().Property(s => s.EnglishName).IsRequired(false);
            builder.Entity<Group>().Property(s => s.Description).IsRequired(false);
            builder.Entity<Group>().Property(s => s.GroupExternalIdentify).IsRequired(false).ValueGeneratedNever();
            builder.Entity<Group>().Property(s => s.CreatedTime).HasDefaultValue(DateTime.Now);
            builder.Entity<Group>().Property(s => s.UpdatedTime).HasDefaultValue(DateTime.Now);

            builder.Entity<GroupPermission>().ToTable("GroupPermissions");
            builder.Entity<GroupPermission>().Property(s => s.GroupId).IsRequired();
            builder.Entity<GroupPermission>().Property(s => s.IsWrite).HasDefaultValue(true).ValueGeneratedNever();
            builder.Entity<GroupPermission>().Property(s => s.IsRead).HasDefaultValue(true).ValueGeneratedNever();
            builder.Entity<GroupPermission>().Property(s => s.CreatedTime).HasDefaultValue(DateTime.Now);
            builder.Entity<GroupPermission>().Property(s => s.UpdatedTime).HasDefaultValue(DateTime.Now);

            builder.Entity<GroupMember>().ToTable("GroupMember");
            builder.Entity<GroupMember>().HasKey("GroupId", "UserId");
            builder.Entity<GroupMember>().Property(s => s.GroupId).IsRequired();
            builder.Entity<GroupMember>().Property(s => s.UserId).IsRequired();

            builder.Entity<Report>().ToTable("Report");
            builder.Entity<Report>().Property(s => s.UserId).IsRequired();
            builder.Entity<Report>().Property(s => s.TargetId).IsRequired();
            builder.Entity<Report>().Property(s => s.TargetType).IsRequired();
            builder.Entity<Report>().Property(s => s.CreatedTime).HasDefaultValue(DateTime.Now);
            builder.Entity<Report>().Property(s => s.UpdatedTime).HasDefaultValue(DateTime.Now);
        }
    }
}

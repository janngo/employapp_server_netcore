﻿using System.Net;

namespace EmpAppBackEnd.Models.Common
{
    public class ApiResponse<T>
    {
        public HttpStatusCode HttpStatus { get; set; }
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
        public T Data { get; set; }
    }
}

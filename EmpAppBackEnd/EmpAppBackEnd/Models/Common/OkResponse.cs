﻿using System.Net;

namespace EmpAppBackEnd.Models.Common
{
    public class OkResponse<T> : ApiResponse<T>
    {
        public OkResponse()
        {
            HttpStatus = HttpStatusCode.OK;
            ErrorMessage = string.Empty;
        }
    }
}

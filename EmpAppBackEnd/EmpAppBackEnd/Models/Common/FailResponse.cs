﻿using System.Net;

namespace EmpAppBackEnd.Models.Common
{
    public class FailResponse<T> : ApiResponse<T>
    {
        public FailResponse()
        {
            HttpStatus = HttpStatusCode.BadRequest;
        }
    }
}

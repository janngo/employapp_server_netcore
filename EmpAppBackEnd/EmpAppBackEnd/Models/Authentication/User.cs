﻿using System.Collections.Generic;

namespace EmpAppBackEnd.Models.Authentication
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Position { get; set; }
        public int GroupId { get; set; }
        public string AvatarUrl { get; set; }
        public List<int> GroupMember { get; set; }
    }
}

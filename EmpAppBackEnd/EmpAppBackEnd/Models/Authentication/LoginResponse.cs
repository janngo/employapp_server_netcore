﻿using System;
using EmpAppBackEnd.Data.DataModel;

namespace EmpAppBackEnd.Models.Authentication
{
    public class LoginResponse
    {
        public string Token { get; set; }
        public DateTime ExpiredDate { get; set; }
        public User User { get; set; }
    }
}

﻿using System.Collections.Generic;

namespace EmpAppBackEnd.Models.Group
{
    public class Group
    {
        public Group()
        {
            Permissions = new List<GroupPermission>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string EnglishName { get; set; }
        public string Description { get; set; }

        public List<GroupPermission> Permissions { get; set; }
    }
}

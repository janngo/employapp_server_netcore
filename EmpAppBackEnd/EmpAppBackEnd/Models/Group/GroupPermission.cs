﻿namespace EmpAppBackEnd.Models.Group
{
    public class GroupPermission
    {
        public int Id { get; set; }
        public int GroupId { get; set; }
        public bool IsRead { get; set; }
        public bool IsWrite { get; set; }
    }
}

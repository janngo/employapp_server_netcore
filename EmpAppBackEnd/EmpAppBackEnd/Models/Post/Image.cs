﻿namespace EmpAppBackEnd.Models.Post
{
    public class Image: PostResource
    {
        public string Url { get; set; }
    }
}

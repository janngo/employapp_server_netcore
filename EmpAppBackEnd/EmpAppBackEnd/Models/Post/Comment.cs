﻿using EmpAppBackEnd.Models.Authentication;

namespace EmpAppBackEnd.Models.Post
{
    public class Comment : PostResource
    {
        public int UserId { get; set; }
        public string Content { get; set; }

        public User User { get; set; }
    }
}

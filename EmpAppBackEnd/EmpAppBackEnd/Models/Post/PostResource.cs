﻿namespace EmpAppBackEnd.Models.Post
{
    public class PostResource
    {
        public int Id { get; set; }
        public int PostId { get; set; }
    }
}

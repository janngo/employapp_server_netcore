﻿using System.Collections.Generic;
using EmpAppBackEnd.Models.Authentication;

namespace EmpAppBackEnd.Models.Post
{
    public class Post
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public int GroupId { get; set; }
        public int CreatedUserId { get; set; }
        public string Tags { get; set; }
        public int CommentCount { get; set; }
        public int InteractCount { get; set; }

        public User User { get; set; }
        public List<Comment> Comments { get; set; }
        public List<Video> Videos { get; set; }
        public List<Image> Images { get; set; }
        public List<Link> Links { get; set; }
    }
}

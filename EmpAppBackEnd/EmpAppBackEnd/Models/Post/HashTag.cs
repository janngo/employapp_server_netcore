﻿namespace EmpAppBackEnd.Models.Post
{
    public class HashTag
    {
        public int Id { get; set; }
        public string Content { get; set; }
    }
}

﻿namespace EmpAppBackEnd.Models.Post
{
    public class Video : PostResource
    {
        public string Url { get; set; }
        public string ThumbnailUrl { get; set; }
    }
}

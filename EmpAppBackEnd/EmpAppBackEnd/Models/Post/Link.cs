﻿namespace EmpAppBackEnd.Models.Post
{
    public class Link : PostResource
    {
        public string Ulr { get; set; }
        public string ThumbnailUrl { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}

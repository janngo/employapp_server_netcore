﻿using EmpAppBackEnd.Data.DataModel;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace EmpAppBackEnd.Controllers
{
    public abstract class BaseController : Controller
    {
        private readonly UserManager<User> _userManager;

        public BaseController(UserManager<User> userManager)
        {
            _userManager = userManager;
        }

        protected UserManager<User> UserManager => _userManager;
    }
}

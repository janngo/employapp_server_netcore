﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using EmpAppBackEnd.Data.DataModel;
using EmpAppBackEnd.Models.Authentication;
using EmpAppBackEnd.Models.Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EmpAppBackEnd.Controllers
{
    [Route("api/auth")]
    public class AccountController : Controller
    {
        private readonly UserManager<Data.DataModel.User> _userManager;
        private readonly RoleManager<UserRole> _roleManager;
        private readonly SignInManager<Data.DataModel.User> _signInManager;
        private readonly ILogger _logger;
        private IPasswordHasher<Data.DataModel.User> _passwordHasher;
        private IConfigurationRoot _configurationRoot;


        public AccountController(
            UserManager<Data.DataModel.User> userManager,
            RoleManager<UserRole> roleManager,
            SignInManager<Data.DataModel.User> signInManager,
            IPasswordHasher<Data.DataModel.User> passwordHasher,
            IConfigurationRoot configurationRoot,
            ILogger<AccountController> logger)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _signInManager = signInManager;
            _passwordHasher = passwordHasher;
            _configurationRoot = configurationRoot;
            _logger = logger;
        }

        [HttpPost]
        [Route("token")]
        public async Task<ApiResponse<LoginResponse>> Login([FromBody] LoginRequest model)
        {
            if(!ModelState.IsValid) return new ApiResponse<LoginResponse>()
            {
                HttpStatus = HttpStatusCode.BadRequest,
                Success = false
            };

            var user = await _userManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                return new ApiResponse<LoginResponse>()
                {
                    HttpStatus = HttpStatusCode.Unauthorized,
                    Success = false
                };
            }
            
            if (_passwordHasher.VerifyHashedPassword(user, user.PasswordHash, model.Password) == PasswordVerificationResult.Success)
            {
                var userClaims = await _userManager.GetClaimsAsync(user);

                var claims = new[]
                {
                    new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                    new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim(JwtRegisteredClaimNames.Email, user.Email),
                }.Union(userClaims).ToList();

                var roleClaims = await _userManager.GetRolesAsync(user);

                if (roleClaims.Any())
                    claims.Union(roleClaims.Select(x => new Claim(ClaimTypes.Role, x)));
                
                var handler = new JwtSecurityTokenHandler();

                var identity = new ClaimsIdentity(
                    new GenericIdentity(user.UserName, "TokenAuth"),
                    claims
                );

                var requestAt = DateTime.Now;
                var expiresIn = requestAt + TokenAuthOption.ExpiresSpan;

                var securityToken = handler.CreateToken(new SecurityTokenDescriptor
                {
                    Issuer = _configurationRoot["JwtSecurityToken:Issuer"],
                    Audience = _configurationRoot["JwtSecurityToken:Audience"],
                    SigningCredentials = TokenAuthOption.SigningCredentials,
                    Subject = identity,
                    Expires = expiresIn
                });
                var token = handler.WriteToken(securityToken);
                
                return  new ApiResponse<LoginResponse>()
                {
                    HttpStatus = HttpStatusCode.Accepted,
                    Success = true,
                    Data = new LoginResponse()
                    {
                        ExpiredDate = securityToken.ValidTo,
                        Token = token,
                        User = new Models.Authentication.User()
                        {
                            Id = user.Id,
                            AvatarUrl = user.Avartar,
                            FirstName = user.FirstName,
                            LastName = user.LastName,
                            Position = user.Position,
                            GroupMember = user.GroupMember!= null && user.GroupMember.Any() ? user.GroupMember.Select(x => x.GroupId).ToList() : new List<int>()
                        }
                    }
                };
            }

            return new ApiResponse<LoginResponse>()
            {
                HttpStatus = HttpStatusCode.Unauthorized,
                Success = false
            };
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("register")]
        public async Task<ApiResponse<object>> Register([FromBody] RegisterRequest model)
        {
            if (!ModelState.IsValid)
            {
                return new ApiResponse<object>()
                {
                    HttpStatus = HttpStatusCode.BadRequest,
                    Success = true,
                };
            }

            var exist = await _roleManager.RoleExistsAsync("Admin");
            if (!exist)
                await _roleManager.CreateAsync(new UserRole("Admin"));

            var user = await _userManager.FindByEmailAsync(model.Email);

            if (user != null)
            {
                await _userManager.AddToRoleAsync(user, "Admin");
                return new ApiResponse<object>()
                {
                    HttpStatus = HttpStatusCode.Found,
                    Success = false,
                    ErrorMessage = "Email is used."
                };
            }

            user = new Data.DataModel.User()
            {
                UserName = model.Email,
                Email = model.Email
            };
            var result = await _userManager.CreateAsync(user, model.Password);
            

            await _userManager.AddToRoleAsync(user, "Admin");

            return new ApiResponse<object>()
            {
                HttpStatus = result.Succeeded ? HttpStatusCode.Accepted : HttpStatusCode.BadRequest,
                Success = result.Succeeded
            };
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using EmpAppBackEnd.Data.Abstract;
using EmpAppBackEnd.Data.DataModel;
using EmpAppBackEnd.Models.Common;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Post = EmpAppBackEnd.Models.Post.Post;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EmpAppBackEnd.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/post")]
    public class PostController : BaseController
    {
        private readonly IPostRepository _postRepository;
        private readonly ICommentRepository _commentRepository;
        private readonly IPostMentionRepository _postMentionRepository;
        private readonly IPostImageRepository _postImageRepository;
        private readonly IPostVideoRepository _postVideoRepository;
        private readonly IPostLinkRepository _postLinkRepository;
        private readonly IHashTagRepository _hashTagRepository;
        private readonly IGroupRepository _groupRepository;

        public PostController(IPostRepository postRepository, ICommentRepository commentRepository,
            IPostMentionRepository postMentionRepository, IPostImageRepository postImageRepository,
            IPostVideoRepository postVideoRepository, IPostLinkRepository postLinkRepository,IHashTagRepository hashTagRepository,IGroupRepository groupRepository,  UserManager<User> userManager) : base(userManager)
        {
            _postRepository = postRepository;
            _commentRepository = commentRepository;
            _postMentionRepository = postMentionRepository;
            _postImageRepository = postImageRepository;
            _postVideoRepository = postVideoRepository;
            _postLinkRepository = postLinkRepository;
            _hashTagRepository = hashTagRepository;
            _groupRepository = groupRepository;
        }

        // GET: api/values
        [HttpGet]
        [Route("getpost")]
        public async Task<ApiResponse<List<Post>>> GetPosts(int group = -1,int page =20,int lastPost =-1)
        {
            throw new NotImplementedException();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ApiResponse<Post> Get(int id)
        {
            try
            {
                var postObj = _postRepository.GetSingle(x => x.Id == id, x => x.User, x => x.Comments, x => x.Images,
                    x => x.Videos, x => x.Links);

                if(postObj == null) return new FailResponse<Post>();

                var post = new Post()
                {
                    Content = postObj.Content,
                    CreatedUserId = postObj.CreatedUserId,
                    GroupId = postObj.GroupId,
                    Id = postObj.Id,
                    Title = postObj.Title,
                    Tags = postObj.Tags
                };

                return new OkResponse<Post>()
                {
                    Data = post
                };
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return new FailResponse<Post>();
        }

        // POST api/values
        [HttpPost]
        public async Task<ApiResponse<Post>> Post([FromBody]Post post)
        {
            if(!ModelState.IsValid) return new FailResponse<Post>(){ErrorMessage = "Invalid Data"};

            var user = await UserManager.GetUserAsync(User);
            if(user == null || post.CreatedUserId != user.Id) return new FailResponse<Post>();
            var group = _groupRepository.GetSingle(x => x.Id == post.GroupId);
            if(group == null) return new FailResponse<Post>();

            var postObj = new Data.DataModel.Post()
            {
                Content = post.Content,
                CreatedTime = DateTime.Now,
                UpdatedTime = DateTime.Now,
                CreatedUserId = post.CreatedUserId,
                GroupId = post.GroupId,
                Tags = post.Tags
            };

            //link
            if (post.Links != null && post.Links.Any())
            {
                foreach (var link in post.Links)
                {
                    var linkObj = new Data.DataModel.PostLink()
                    {
                        Url = link.Ulr
                    };
                    postObj.Links.Add(linkObj);
                }
            }

            if (post.Images != null && post.Images.Any())
            {
                foreach (var image in post.Images)
                {
                    try
                    {
                        var bytes = Convert.FromBase64String(image.Url);
                        var filename = $"/Upload/{Guid.NewGuid()}.jpg";

                        var filepath = Path.Combine($"{Environment.CurrentDirectory}{filename}");
                        var imageObj = new Data.DataModel.PostImage()
                        {
                            CreatedTime = DateTime.Now,
                            Path = filepath,
                            UpdatedTime = DateTime.Now,
                        };

                        System.IO.File.WriteAllBytes(filepath,bytes);

                        postObj.Images.Add(imageObj);
                    }
                    catch (Exception e)
                    {
                        
                    }
                }
            }

            var result = _postRepository.Add(postObj);
            _postRepository.Commit();
            post.Id = result.Id;

            return new OkResponse<Post>(){Data = post};
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EmpAppBackEnd.Data.Abstract;
using EmpAppBackEnd.Data.DataModel;
using EmpAppBackEnd.Models.Common;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Group = EmpAppBackEnd.Models.Group.Group;
using GroupPermission = EmpAppBackEnd.Models.Group.GroupPermission;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EmpAppBackEnd.Controllers
{
    [Route("api/group")]
    public class GroupController : BaseController
    {
        private readonly IGroupRepository _groupRepository;

        public GroupController(IGroupRepository groupRepository,UserManager<User> userManager) : base(userManager)
        {
            _groupRepository = groupRepository;
        }
        // GET: api/values
        [HttpGet]
        [Route("getall")]
        public async Task<ApiResponse<List<Group>>> GetAll(string language = "de")
        {
            try
            {
                var postObj = await _groupRepository.AllIncluding(x => x.GroupPermissions).ToListAsync();

                List<Group> postDto = null;

                if (postObj != null && postObj.Any())
                {
                    postDto = postObj.Select(x => new Group()
                    {
                        Id = x.Id,
                        Description = x.Description,
                        Name = string.Equals(language, "de") ? x.Name : x.EnglishName,
                        Permissions = x.GroupPermissions != null && x.GroupPermissions.Any()
                            ? x.GroupPermissions.Select(p => new GroupPermission()
                            {
                                Id = p.Id,
                                GroupId = p.GroupId,
                                IsWrite = p.IsWrite,
                                IsRead = p.IsRead
                            }).ToList()
                            : null
                    }).ToList();
                }

                return new OkResponse<List<Group>>()
                {
                    Data = postDto
                };
            }
            catch (Exception e)
            {
                
            }
            
            return new FailResponse<List<Group>>();
        }

        // POST api/values
        [HttpPost]
        public ApiResponse<object> Post([FromBody]Group group)
        {
            try
            {
                var groupObj = new Data.DataModel.Group()
                {
                    Id = group.Id,
                    CreatedTime = DateTime.Now,
                    Description = group.Description,
                    EnglishName = group.EnglishName,
                    GroupExternalIdentify = null,
                    Name = group.Name,
                    UpdatedTime = DateTime.Now
                };


                if (group.Permissions != null && group.Permissions.Any())
                {
                    foreach (var pe in group.Permissions)
                    {
                        groupObj.GroupPermissions.Add(new Data.DataModel.GroupPermission()
                        {
                            IsRead = pe.IsRead,
                            IsWrite = pe.IsWrite,
                        });
                    }
                }
                _groupRepository.Add(groupObj);
                _groupRepository.Commit();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return new FailResponse<object>();
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public ApiResponse<object> Put(int id, [FromBody]Group group)
        {
            if(!ModelState.IsValid || id != group.Id) return new FailResponse<object>(){ErrorMessage = "Invalid Data"};

            try
            {
                var groupObj = _groupRepository.GetSingle(x => x.Id == id,x => x.GroupPermissions);

                if (groupObj != null)
                {
                    groupObj.Description = group.Description;
                    groupObj.Name = group.Name;
                    groupObj.EnglishName = group.EnglishName;

                    var removeList = new List<Data.DataModel.GroupPermission>();

                    foreach (var g in groupObj.GroupPermissions)
                    {
                        var contain = group.Permissions.Any(x => x.Id == g.Id);
                        if(!contain) removeList.Add(g);
                    }

                    foreach (var r in removeList)
                    {
                        groupObj.GroupPermissions.Remove(r);
                    }

                    foreach (var g in group.Permissions)
                    {
                        var updateGroup = groupObj.GroupPermissions.FirstOrDefault(x => x.Id == g.Id);

                        if (updateGroup != null)
                        {
                            updateGroup.IsRead = g.IsRead;
                            updateGroup.IsWrite = g.IsWrite;
                            updateGroup.UpdatedTime = DateTime.Now;
                        }
                        else
                        {
                            updateGroup = new Data.DataModel.GroupPermission()
                            {
                                IsWrite = g.IsWrite,
                                IsRead = g.IsRead,
                                CreatedTime = DateTime.Now,
                                UpdatedTime = DateTime.Now
                            };

                            groupObj.GroupPermissions.Add(updateGroup);
                        }
                    }

                    _groupRepository.Update(groupObj);

                    return new OkResponse<object>();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return new FailResponse<object>();
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public ApiResponse<object> Delete(int id)
        {
            try
            {
                var group = _groupRepository.GetSingle(x => x.Id == id, x => x.GroupPermissions);

                if (group != null)
                {
                    _groupRepository.Delete(group);
                    return new OkResponse<object>();
                }
            }
            catch (Exception e)
            {
                
            }

            return new FailResponse<object>();
        }
    }
}

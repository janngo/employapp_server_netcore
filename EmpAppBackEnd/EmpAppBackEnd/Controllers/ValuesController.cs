﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EmpAppBackEnd.Data.DataModel;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace EmpAppBackEnd.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    public class ValuesController : BaseController
    {
        private readonly RoleManager<UserRole> _roleManager;

        public ValuesController(UserManager<User> userManager, RoleManager<UserRole> roleManager) : base(userManager)
        {
            _roleManager = roleManager;
        }
        // GET api/values
        [HttpGet]
        public async Task< IEnumerable<string>> Get()
        {
            var user = await UserManager.GetUserAsync(User);
            
            var id = UserManager.GetUserId(User);
            var b = await UserManager.IsInRoleAsync(user, "Admin");
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}

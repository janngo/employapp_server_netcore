﻿// <auto-generated />
using EmpAppBackEnd.Data.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Storage.Internal;
using System;

namespace EmpAppBackEnd.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    partial class ApplicationDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn)
                .HasAnnotation("ProductVersion", "2.0.0-rtm-26452");

            modelBuilder.Entity("EmpAppBackEnd.Data.DataModel.Comment", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Content")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.Property<DateTime>("CreatedTime")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(new DateTime(2017, 9, 16, 22, 42, 49, 888, DateTimeKind.Local));

                    b.Property<int>("PostId");

                    b.Property<DateTime>("UpdatedTime")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(new DateTime(2017, 9, 16, 22, 42, 49, 888, DateTimeKind.Local));

                    b.Property<int>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("PostId");

                    b.HasIndex("UserId");

                    b.ToTable("Comment");
                });

            modelBuilder.Entity("EmpAppBackEnd.Data.DataModel.Group", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedTime")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(new DateTime(2017, 9, 16, 22, 42, 49, 892, DateTimeKind.Local));

                    b.Property<string>("Description");

                    b.Property<string>("EnglishName");

                    b.Property<string>("GroupExternalIdentify");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<DateTime>("UpdatedTime")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(new DateTime(2017, 9, 16, 22, 42, 49, 892, DateTimeKind.Local));

                    b.HasKey("Id");

                    b.ToTable("Group");
                });

            modelBuilder.Entity("EmpAppBackEnd.Data.DataModel.GroupMember", b =>
                {
                    b.Property<int>("GroupId");

                    b.Property<int>("UserId");

                    b.HasKey("GroupId", "UserId");

                    b.HasIndex("UserId");

                    b.ToTable("GroupMember");
                });

            modelBuilder.Entity("EmpAppBackEnd.Data.DataModel.GroupPermission", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedTime")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(new DateTime(2017, 9, 16, 22, 42, 49, 892, DateTimeKind.Local));

                    b.Property<int>("GroupId");

                    b.Property<bool>("IsRead")
                        .HasDefaultValue(true);

                    b.Property<bool>("IsWrite")
                        .HasDefaultValue(true);

                    b.Property<DateTime>("UpdatedTime")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(new DateTime(2017, 9, 16, 22, 42, 49, 892, DateTimeKind.Local));

                    b.HasKey("Id");

                    b.HasIndex("GroupId");

                    b.ToTable("GroupPermissions");
                });

            modelBuilder.Entity("EmpAppBackEnd.Data.DataModel.HashTag", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Content")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<DateTime>("CreatedTime")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(new DateTime(2017, 9, 16, 22, 42, 49, 891, DateTimeKind.Local));

                    b.Property<int?>("PostHashtTagHashTagId");

                    b.Property<int?>("PostHashtTagPostId");

                    b.Property<DateTime>("UpdatedTime")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(new DateTime(2017, 9, 16, 22, 42, 49, 891, DateTimeKind.Local));

                    b.Property<int>("UsedNo");

                    b.HasKey("Id");

                    b.HasIndex("PostHashtTagPostId", "PostHashtTagHashTagId");

                    b.ToTable("HashTag");
                });

            modelBuilder.Entity("EmpAppBackEnd.Data.DataModel.Post", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CommentCount");

                    b.Property<string>("Content")
                        .IsRequired()
                        .HasMaxLength(1000);

                    b.Property<DateTime>("CreatedTime")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(new DateTime(2017, 9, 16, 22, 42, 49, 887, DateTimeKind.Local));

                    b.Property<int>("CreatedUserId");

                    b.Property<int>("GroupId");

                    b.Property<int>("InteractCount");

                    b.Property<bool>("IsPublish")
                        .HasDefaultValue(true);

                    b.Property<string>("Tags");

                    b.Property<string>("Title")
                        .HasMaxLength(100);

                    b.Property<DateTime>("UpdatedTime")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(new DateTime(2017, 9, 16, 22, 42, 49, 888, DateTimeKind.Local));

                    b.Property<int?>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("Post");
                });

            modelBuilder.Entity("EmpAppBackEnd.Data.DataModel.PostHashtTag", b =>
                {
                    b.Property<int>("PostId");

                    b.Property<int>("HashTagId");

                    b.HasKey("PostId", "HashTagId");

                    b.ToTable("PostHashtTag");
                });

            modelBuilder.Entity("EmpAppBackEnd.Data.DataModel.PostImage", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedTime")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(new DateTime(2017, 9, 16, 22, 42, 49, 889, DateTimeKind.Local));

                    b.Property<string>("Path")
                        .IsRequired();

                    b.Property<int>("PostId");

                    b.Property<DateTime>("UpdatedTime")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(new DateTime(2017, 9, 16, 22, 42, 49, 889, DateTimeKind.Local));

                    b.HasKey("Id");

                    b.HasIndex("PostId");

                    b.ToTable("PostImage");
                });

            modelBuilder.Entity("EmpAppBackEnd.Data.DataModel.PostLink", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedTime")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(new DateTime(2017, 9, 16, 22, 42, 49, 890, DateTimeKind.Local));

                    b.Property<string>("Description");

                    b.Property<int>("PostId");

                    b.Property<string>("ThumbnailPath");

                    b.Property<string>("Title");

                    b.Property<DateTime>("UpdatedTime")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(new DateTime(2017, 9, 16, 22, 42, 49, 890, DateTimeKind.Local));

                    b.Property<string>("Url")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("PostId");

                    b.ToTable("PostLink");
                });

            modelBuilder.Entity("EmpAppBackEnd.Data.DataModel.PostMention", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedTime")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(new DateTime(2017, 9, 16, 22, 42, 49, 889, DateTimeKind.Local));

                    b.Property<int>("PostId");

                    b.Property<DateTime>("UpdatedTime")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(new DateTime(2017, 9, 16, 22, 42, 49, 889, DateTimeKind.Local));

                    b.Property<int>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("PostMention");
                });

            modelBuilder.Entity("EmpAppBackEnd.Data.DataModel.PostReaction", b =>
                {
                    b.Property<int>("PostId");

                    b.Property<int>("UserId");

                    b.Property<int>("Id");

                    b.HasKey("PostId", "UserId");

                    b.HasIndex("UserId");

                    b.ToTable("PostReaction");
                });

            modelBuilder.Entity("EmpAppBackEnd.Data.DataModel.PostVideo", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedTime")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(new DateTime(2017, 9, 16, 22, 42, 49, 889, DateTimeKind.Local));

                    b.Property<string>("Path")
                        .IsRequired();

                    b.Property<int>("PostId");

                    b.Property<string>("ThumbnailPath");

                    b.Property<DateTime>("UpdatedTime")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(new DateTime(2017, 9, 16, 22, 42, 49, 889, DateTimeKind.Local));

                    b.HasKey("Id");

                    b.HasIndex("PostId");

                    b.ToTable("PostVideo");
                });

            modelBuilder.Entity("EmpAppBackEnd.Data.DataModel.Report", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Comment")
                        .HasMaxLength(200);

                    b.Property<DateTime>("CreatedTime")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(new DateTime(2017, 9, 16, 22, 42, 49, 893, DateTimeKind.Local));

                    b.Property<int>("TargetId");

                    b.Property<int>("TargetType");

                    b.Property<DateTime>("UpdatedTime")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(new DateTime(2017, 9, 16, 22, 42, 49, 893, DateTimeKind.Local));

                    b.Property<int>("UserId");

                    b.HasKey("Id");

                    b.ToTable("Report");
                });

            modelBuilder.Entity("EmpAppBackEnd.Data.DataModel.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("Avartar");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Email")
                        .HasMaxLength(256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<string>("FirstName")
                        .HasMaxLength(30);

                    b.Property<string>("LastName")
                        .HasMaxLength(30);

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("Position")
                        .HasMaxLength(50);

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex");

                    b.ToTable("AspNetUsers");
                });

            modelBuilder.Entity("EmpAppBackEnd.Data.DataModel.UserRole", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasName("RoleNameIndex");

                    b.ToTable("AspNetRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<int>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<int>("RoleId");

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<int>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<int>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<int>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<int>("UserId");

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<int>", b =>
                {
                    b.Property<int>("UserId");

                    b.Property<int>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<int>", b =>
                {
                    b.Property<int>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("EmpAppBackEnd.Data.DataModel.Comment", b =>
                {
                    b.HasOne("EmpAppBackEnd.Data.DataModel.Post")
                        .WithMany("Comments")
                        .HasForeignKey("PostId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("EmpAppBackEnd.Data.DataModel.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("EmpAppBackEnd.Data.DataModel.GroupMember", b =>
                {
                    b.HasOne("EmpAppBackEnd.Data.DataModel.Group", "Group")
                        .WithMany()
                        .HasForeignKey("GroupId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("EmpAppBackEnd.Data.DataModel.User")
                        .WithMany("GroupMember")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("EmpAppBackEnd.Data.DataModel.GroupPermission", b =>
                {
                    b.HasOne("EmpAppBackEnd.Data.DataModel.Group")
                        .WithMany("GroupPermissions")
                        .HasForeignKey("GroupId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("EmpAppBackEnd.Data.DataModel.HashTag", b =>
                {
                    b.HasOne("EmpAppBackEnd.Data.DataModel.PostHashtTag")
                        .WithMany("HashTags")
                        .HasForeignKey("PostHashtTagPostId", "PostHashtTagHashTagId");
                });

            modelBuilder.Entity("EmpAppBackEnd.Data.DataModel.Post", b =>
                {
                    b.HasOne("EmpAppBackEnd.Data.DataModel.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("EmpAppBackEnd.Data.DataModel.PostHashtTag", b =>
                {
                    b.HasOne("EmpAppBackEnd.Data.DataModel.Post")
                        .WithMany("HashTags")
                        .HasForeignKey("PostId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("EmpAppBackEnd.Data.DataModel.PostImage", b =>
                {
                    b.HasOne("EmpAppBackEnd.Data.DataModel.Post")
                        .WithMany("Images")
                        .HasForeignKey("PostId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("EmpAppBackEnd.Data.DataModel.PostLink", b =>
                {
                    b.HasOne("EmpAppBackEnd.Data.DataModel.Post")
                        .WithMany("Links")
                        .HasForeignKey("PostId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("EmpAppBackEnd.Data.DataModel.PostMention", b =>
                {
                    b.HasOne("EmpAppBackEnd.Data.DataModel.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("EmpAppBackEnd.Data.DataModel.PostReaction", b =>
                {
                    b.HasOne("EmpAppBackEnd.Data.DataModel.Post")
                        .WithMany("Reactions")
                        .HasForeignKey("PostId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("EmpAppBackEnd.Data.DataModel.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("EmpAppBackEnd.Data.DataModel.PostVideo", b =>
                {
                    b.HasOne("EmpAppBackEnd.Data.DataModel.Post")
                        .WithMany("Videos")
                        .HasForeignKey("PostId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<int>", b =>
                {
                    b.HasOne("EmpAppBackEnd.Data.DataModel.UserRole")
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<int>", b =>
                {
                    b.HasOne("EmpAppBackEnd.Data.DataModel.User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<int>", b =>
                {
                    b.HasOne("EmpAppBackEnd.Data.DataModel.User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<int>", b =>
                {
                    b.HasOne("EmpAppBackEnd.Data.DataModel.UserRole")
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("EmpAppBackEnd.Data.DataModel.User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<int>", b =>
                {
                    b.HasOne("EmpAppBackEnd.Data.DataModel.User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
